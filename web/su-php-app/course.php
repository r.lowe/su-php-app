<?php
require_once 'model.php';

class Course extends Model {

  public function __construct($attrs) {
    parent::__construct($attrs);
    $this->url = $this->root_url()."/courses/".$this->id.".json";
    $this->department_url = $this->root_url()."/departments/".$this->id.".json";
  }

}
