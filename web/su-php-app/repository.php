<?php
require_once 'course.php';
require_once 'department.php';

class Repository {

  private $db_host     = "localhost";
  private $db_username = "courseapi";
  private $db_password = "fishfood";
  private $db_name     = "prospectus";

  public function __construct($klass) {
    $this->klass = $klass;
  }

  public function find($id) {
    $sql = "SELECT * FROM ".$this->table()." WHERE id=$id";
    if ($result = $this->query($sql)) {
      return new $this->klass($result->fetch_assoc());
    } else {
      return null;
    }
  }

  public function find_all() {
    $sql = "SELECT * FROM ".$this->table();
    if ($result = $this->query($sql)) {
      $instances = array();
      while ($instance_data = $result->fetch_assoc()) {
        $instances[] = new $this->klass($instance_data);
      }
      return $instances;
    } else {
      return null;
    }
  }

  public function find_all_by($attr, $id) {
    $sql = "SELECT * FROM ".$this->table()." WHERE $attr=$id";
    if ($result = $this->query($sql)) {
      $instances = array();
      while ($instance_data = $result->fetch_assoc()) {
        $instances[] = new $this->klass($instance_data);
      }
      return $instances;
    } else {
      return null;
    }
  }

  private function table() {
    return strtolower($this->klass)."s";
  }

  private function query($sql) {
    $connection = new mysqli($this->db_host, $this->db_username, $this->db_password, $this->db_name);
    if ($connection->connect_error) {
      die("Failed to connect to database: ".$connection->connect_error);
    }
    if ($result = $connection->query($sql)) {
      $connection->close();
      return $result;
    } else {
      echo "Failed: ".mysqli_error($connection);
    }
    $connection->close();
  }

}
