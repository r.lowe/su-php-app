<?php

abstract class Model {
  
  public function __construct($attrs) {
    foreach (array_keys($attrs) as $key) {
      $this->$key = $attrs[$key];
    }
  }

  protected function root_url() {
    return "http://".$_SERVER['SERVER_NAME'];
  }

}
