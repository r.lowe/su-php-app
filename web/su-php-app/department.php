<?php
require_once 'model.php';

class Department extends Model {

  public function __construct($attrs) {
    parent::__construct($attrs);
    $this->courses_url = $this->root_url()."/departments/".$this->id."/courses.json";
  }
}
