-- MySQL dump 10.13  Distrib 5.5.20, for osx10.10 (x86_64)
--
-- Host: localhost    Database: prospectus
-- ------------------------------------------------------
-- Server version	5.5.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `qualification` varchar(3) NOT NULL,
  `ucas_code` varchar(4) NOT NULL,
  `duration` int(11) NOT NULL,
  `a_levels` varchar(3) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'Computer Science',1,'BSc','G402',3,'AAB','You\'ll learn to understand the theoretical principles underlying a problem, and how to engineer a solution. You\'ll also become familiar with the practical issues involved in developing reliable, effective software systems in business or industry. \n\nAs well as learning to program and think analytically, you\'ll be encouraged to work in teams and develop your communication skills. In the third year, half of your time is spent on a substantial individual project, giving you scope for creative and intellectual input.'),(2,'Artificial Intelligence and Computer Science',1,'BSc','GG74',3,'AAB','Our degrees will help you to become more than just a programmer - you will also develop skills in teamwork, communication, systems design and entrepreneurship. \n\nThis course is about biologically inspired algorithms, their relationship to living biological intelligence and the nature of consciousness itself. Some modules overlap with the main Computer Science degree, so you get the same solid grounding in the fundamentals. \n\nYou\'ll get the chance to specialise in speech recognition, language processing or robotics. We encourage you to take optional modules in psychology and philosophy to enhance your understanding.'),(3,'Physics and Astrophysics',2,'MPh','F3F5',4,'AAA','This is a broad and intellectually stimulating degree that includes core physics knowledge and a thorough grounding in astrophysics. You\'ll study the workings of the universe, from the planets of our solar system to the most distant galaxies. \n\nYou\'ll learn how to analyse astronomical data, draw conclusions and present results. You can spend time in the astronomy laboratory and use our robotic telescope to take your own data. In your final year, you\'ll complete a project in an area that interests you. \n\nEach year, one or two students on this course may be selected to spend their final year working at the Isaac Newton Group of Telescopes on La Palma. This provides experience of a working observatory and the chance to complete an extended research project.');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'Computer Science'),(2,'Physics and Astronomy');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-17 13:27:00
