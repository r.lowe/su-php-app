<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require_once 'repository.php';

$config = [
    'settings' => [
        'displayErrorDetails' => true,]
    ];

function throw404($response) {
  $response = $response->withStatus(404, "Not found");
  $response->getBody()->write("Not found");
}

function find($klass, Request $request, Response $response) {
  $repository = new Repository($klass);
  $instance = $repository->find($request->getAttribute('id'));
  output($instance, $request, $response);
}

function output($data, Request $request, Response $response) {
  if ($data) {
    switch ($request->getAttribute('format')) {
    case "json":
      $response->getBody()->write(json_encode($data));
      break;
    default:
      throw404($response);
    }
  } else {
    throw404($response);
  }
  return $response;
}

function find_all($klass, Request $request, Response $response) {
  $repository = new Repository($klass);
  $instances = $repository->find_all();
  output($instances, $request, $response);
}

function find_all_by($klass, $attr, Request $request, Response $response) {
  $repository = new Repository($klass);
  $instances = $repository->find_all_by($attr, $request->getAttribute('id'));
  output($instances, $request, $response);
}

$app = new \Slim\App();

$app->get('/courses/{id}.{format}', function (Request $request, Response $response) {
  return find("Course", $request, $response);
});

$app->get('/courses.{format}', function (Request $request, Response $response) {
  return find_all("Course", $request, $response);
});

$app->get('/departments.{format}', function (Request $request, Response $response) {
  return find_all("Department", $request, $response);
});

$app->get('/departments/{id}.{format}', function (Request $request, Response $response) {
  return find("Department", $request, $response);
});

$app->get('/departments/{id}/courses.{format}', function (Request $request, Response $response) {
  return find_all_by("Course", "department_id", $request, $response);
});
$app->run();
