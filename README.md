su-php-app

Note:--	I have set the volumes to be use the actual directory, 	so from now changes to any php with slim can be made. 
Just refresh the broswer and to lookafter the database you have phpmyadmin.
	
####### ##### #######

Docker install:-- Please go to the link and select the platform you are using

		https://docs.docker.com/install/#supported-platforms
	
Now just follow the instructions for desired platform

####### ##### #######

Docker-compose install:-- Please go to the link and select the platform you are using
	
		https://docs.docker.com/compose/install/

Now just follow the instructions for desired platform

####### ##### #######

Setting up:-- So now you have docker and ran the hello-world and docker compose is also installed.
	Here you need to terminals at the same directory position as you have pulled this from git
	
Terminal 1:-- change into directory su-php-app, this should contain "docker-compose.yml"
type the following.....

	docker-compose up --build

this will build and stay connected for viewing messages

Terminal 2:-- change into directory su-php-app, this should contain "docker-compose.yml"
type the following.....

	docker ps

you will see the 3 containers running a php-apache, mariadb and phpadmin
find the name of the apache container it will be like "su-php-app_web_1"
type the following replacing N4M3 with the container name.....

	docker exec -it N4M3 /bin/bash

now last few commands needed on the host you have connected
type.....

	composer install --prefer-source --no-interaction

	composer require slim/slim "^3.0"

now you can disconnect with ctrl+d
all done

####### ##### #######
	
Browser tests:-- now to your browser of choice and do to the following
web:--

	http://localhost:19826/index.html

phpmyadmin:--
	
	http://localhost:19827/
	uid 	= courseapi
	passwd 	= fishfood

####### ##### #######

Stopping containers:-- Finished working on code then time to stop the containers
Terminal 1:-- you can disconnect with 

    ctrl+d

####### ##### #######

Starting containers:-- Finished working on code then time to stop the containers
Terminal 1:-- change into directory su-php-app, this should contain "docker-compose.yml"
type the following.....

	docker-compose up

this will run and stay connected for viewing messages

####### ##### #######

Starting fresh containers:-- So want new fresh containers then please 
Terminal 1:-- change into directory su-php-app, this should contain "docker-compose.yml"
type the following.....

	yes | docker-compose rm

	docker-compose up --build

this will build and stay connected for viewing messages